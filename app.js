const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
require('dotenv').config();
const app = express();
const {
  mainErrHandler,
  dbConnect,
  badReq} = require('./middleware/helpers');
dbConnect(mongoose, mongoose.connection);
const authRouter = require('./routes/authRouter');
const meRouter = require('./routes/meRouter');
const notesRouter = require('./routes/notesRouter');

app.use(morgan('dev'));
app.use(express.json());
app.use('/api/auth', authRouter);
app.use('/api/users', meRouter);
app.use('/api/notes', notesRouter);
app.use('*', badReq);
app.use(mainErrHandler);


module.exports = app;


