const httpResponseStatus = {
  success: 200,
  badRequest: 400,
  serverError: 500,
  notFound: 404,
  authorizationFailed: 401,
};
const passwordRegExp = '^[a-zA-Z0-9]{6,30}$';
const limitDefault = 5;
const offsetDefault = 0;
const limitBarrier = 100;
const existErrorCode = 11000;
const encodeLimit = 10;
const minUsernameLength = 3;
const maxUsernameLength = 30;
const otherPort = 8087;


module.exports = {
  httpResponseStatus,
  passwordRegExp,
  limitDefault,
  offsetDefault,
  limitBarrier,
  existErrorCode,
  encodeLimit,
  minUsernameLength,
  maxUsernameLength,
  otherPort,
}
;
