const User = require('../models/user');
const bCrypt = require('bcryptjs');
const JWT = require('jsonwebtoken');
const {httpResponseStatus: {
  success,
  badRequest,
}, existErrorCode, encodeLimit} = require('../constants');

const registration = async (req, res) => {
  const {username, password} = req.body;
  try {
    const user = new User({
      username,
      password: await bCrypt.hash(password, encodeLimit),
    });

    await user.save();
    res.status(success).json({message: 'User has been saved'});
  } catch (err) {
    if (err.code === existErrorCode) {
      return res.status(badRequest).json({
        message: 'User with this name already exist',
      });
    }
  }
};

const login = async (req, res) => {
  const {username, password} = req.body;
  const user = await User.findOne({username});

  if (!user) {
    return res.status(badRequest).json({
      message: `No such user with ${username} has been found!`,
    });
  }

  const isValid = await bCrypt.compare(password, user.password);

  if (!isValid) {
    return res.status(badRequest).json({message: `Invalid password!`});
  }

  const {_id} = user;
  // eslint-disable-next-line camelcase
  const jwt_token = JWT.sign({
    _id,
    username,
  }, process.env.JWT_SECRET);

  res.status(success).json({
    message: 'Welcome Boss',
    jwt_token,
  });
};

module.exports={
  registration,
  login,
}
;
