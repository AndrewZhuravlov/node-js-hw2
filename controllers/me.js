const User = require('../models/user');
const Note = require('../models/note');
const bCrypt = require('bcryptjs');
const {httpResponseStatus: {
  success,
  badRequest,
}, encodeLimit} = require('../constants');

const getMe = async (req, res) => {
  const _id = req.userInfo._id;
  const user = await User.findById(_id);
  const {username, createdDate} = user;

  res.status(success).json({
    user: {
      _id,
      username,
      createdDate,
    },
  });
};

const deleteMe = async (req, res) => {
  const _id = req.userInfo._id;

  await User.findByIdAndDelete(_id);
  await Note.deleteMany({userId: _id});
  res.status(success).json({message: 'User has been deleted.'});
};

const changePassword = async (req, res) => {
  const {_id} = req.userInfo;
  const {oldPassword, newPassword} = req.body;
  const user = await User.findById(_id);
  const isValidPassword = await bCrypt.compare(oldPassword, user.password);

  if (!isValidPassword) {
    return res.status(badRequest).json({message: 'Invalid old password.'});
  }

  user.password = await bCrypt.hash(newPassword, encodeLimit);
  await user.save();
  res.status(success).json({message: 'Password has been changed.'});
};

module.exports = {
  getMe,
  deleteMe,
  changePassword,
}
;
