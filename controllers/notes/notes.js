const {
  httpResponseStatus: {success},
  limitBarrier,
  limitDefault,
  offsetDefault,
} = require('../../constants');
const Note = require('../../models/note');

const createNote = async (req, res) => {
  const {text} = req.body;
  const userId = req.userInfo._id;
  const note = new Note({
    userId,
    text,
  });

  await note.save();
  res.status(success).json({message: 'Note has been created'});
};

const getAllUserNotes = async (req, res) => {
  const userId = req.userInfo._id;
  const {limit = limitDefault, offset = offsetDefault} = req.query;
  const userNotes = await Note.find({userId}, {__v: 0}, {
    limit: +limit > limitBarrier ? limitDefault : +limit,
    skip: +offset,
    sort: {
      createdDate: -1,
    },
  });

  res.status(success).json({
    notes: userNotes,
    limit,
    offset: +limit + +offset,
  });
};


module.exports = {
  createNote,
  getAllUserNotes,
};

