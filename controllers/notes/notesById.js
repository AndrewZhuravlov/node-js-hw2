const Note = require('../../models/note');
const {validateObjectId} = require('../../middleware/helpers');
const {httpResponseStatus: {
  success,
  badRequest,
}} = require('../../constants');

const getUsersNoteById = async (req, res) => {
  const noteId = req.params.id;

  if (!validateObjectId(noteId) ) {
    return res.status(badRequest).json({message: 'Invalid ID'});
  }

  if (!await Note.exists({_id: noteId})) {
    return res.status(badRequest).json({message: 'No such note was found.'});
  }

  const note = await Note.findOne({_id: noteId}, {__v: false});
  res.status(success).json({note});
};

const updateNote = async (req, res) => {
  const noteId = req.params.id;
  const {text} = req.body;
  const note = await Note.findOne({_id: noteId});
  note.text = text;
  await note.save();
  res.status(success).json({message: 'Note has been updated'});
};

const checkUncheckNote = async (req, res) => {
  const noteId = req.params.id;

  await Note.updateOne({_id: noteId}, {completed: !this.completed});
  res.status(success).json({message: 'Note has been checked/unchecked'});
};

const deleteNote = async (req, res) => {
  const noteId = req.params.id;

  await Note.findByIdAndDelete({_id: noteId});
  res.status(success).json({message: 'Note has been deleted'});
};

module.exports = {
  getUsersNoteById,
  updateNote,
  checkUncheckNote,
  deleteNote,
}
;
