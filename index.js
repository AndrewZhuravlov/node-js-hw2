const app = require('./app');
const {otherPort} = require('./constants');

const port = process.env.PORT || otherPort;

app.listen(port, () => console.log('Server has been started'));
