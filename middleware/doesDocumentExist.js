const {httpResponseStatus: {
  badRequest,
}} = require('../constants');
const {validateObjectId} = require('./helpers');

module.exports = (Model, note = false) => async (req, res, next) => {
  const userId = req.userInfo._id;
  const noteId = req.params.id;

  if (note) {
    if (!validateObjectId(noteId) || !validateObjectId(userId)) {
      return res.status(badRequest).json({message: 'Invalid ID'});
    }

    if (!await Model.exists({userId, _id: noteId})) {
      return res.status(badRequest).json({message: 'No such note was found.'});
    }

    next();
  } else {
    if (!validateObjectId(userId)) {
      return res.status(badRequest).json({message: 'Invalid ID'});
    }

    if (!await Model.exists({_id: userId})) {
      return res.status(badRequest).json({message: 'No such user was found.'});
    }

    next();
  }
}
;
