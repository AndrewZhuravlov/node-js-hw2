const mongoose = require('mongoose');
const {Types: {ObjectId}} = mongoose;
const {httpResponseStatus: {
  serverError,
  notFound,
}} = require('../constants');
const asyncErrHandler = (fn) => (req, res, next) => {
  return Promise
      .resolve(fn(req, res, next))
      .catch(next);
};

const mainErrHandler = (error, req, res, next) => {
  res.status(error.status || serverError);
  res.json({
    status: error.status,
    message: error.message,
    stack: error.stack,
  });
};

const dbConnectionChecker = (db) => {
  db.on('error', console.error.bind(console, 'connection error:'));
  db.once('open', () => console.log('db has been connected'));
};

const badReq = (req, res) => {
  return res.status(notFound).json({message: '404! Not found'});
};

const dbConnect = (mongoose, db) => {
  const URI = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASSWORD}@cluster0.bc5og.mongodb.net/myApp`;

  mongoose.connect(URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  });

  dbConnectionChecker(db);
};


const validateObjectId = (id) => ObjectId.isValid(id) &&
   (new ObjectId(id)).toString() === id;


module.exports = {
  asyncErrHandler,
  mainErrHandler,
  dbConnectionChecker,
  badReq,
  dbConnect,
  validateObjectId,
}
;
