const Joi = require('joi');
const {httpResponseStatus: {
  badRequest,
}, passwordRegExp} = require('../constants');

module.exports = async function(req, res, next) {
  const schema = Joi.object({
    oldPassword: Joi.string()
        .pattern(new RegExp(passwordRegExp))
        .required(),
    newPassword: Joi.string()
        .pattern(new RegExp(passwordRegExp))
        .required(),
  });

  await schema.validateAsync(req.body)
      .then(next())
      .catch((err) => {
        return res.status(badRequest).json({message: err.message});
      });
};
