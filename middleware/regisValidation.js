const Joi = require('joi');
const {
  httpResponseStatus: {badRequest},
  passwordRegExp,
  minUsernameLength,
  maxUsernameLength} = require('../constants');

module.exports = async function(req, res, next) {
  const schema = Joi.object({
    username: Joi.string()
        .min(minUsernameLength)
        .max(maxUsernameLength)
        .required(),
    password: Joi.string()
        .pattern(new RegExp(passwordRegExp)),
  });
  await schema.validateAsync(req.body)
      .then(next())
      .catch((err) => {
        return res.status(badRequest).json({message: err.message});
      });
};

