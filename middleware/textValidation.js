const Joi = require('joi');
const {httpResponseStatus: {
  badRequest,
}} = require('../constants');

module.exports = async function(req, res, next) {
  const schema = Joi.object({
    text: Joi.string()
        .required(),
  });

  await schema.validateAsync(req.body)
      .then(next())
      .catch((err) => {
        return res.status(badRequest).json({message: err.message});
      });
};

