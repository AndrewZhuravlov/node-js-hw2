/* eslint-disable new-cap */
const {Schema, model} = require('mongoose');
const schema = Schema({
  username: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = model('User', schema);
