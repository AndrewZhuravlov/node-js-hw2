/* eslint-disable new-cap */
const {Router} = require('express');
const route = Router();
const {login, registration} = require('../controllers/authUser');
const {asyncErrHandler} = require('../middleware/helpers');
const regisValidation = require('../middleware/regisValidation');


route.post('/login',
    asyncErrHandler(regisValidation),
    asyncErrHandler(login));
route.post('/register',
    asyncErrHandler(regisValidation),
    asyncErrHandler(registration));

module.exports = route;
