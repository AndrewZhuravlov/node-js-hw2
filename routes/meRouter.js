/* eslint-disable new-cap */
const {Router} = require('express');
const route = Router();
const {getMe, deleteMe, changePassword} = require('../controllers/me');
const User = require('../models/user');
const doesDocumentExist = require('../middleware/doesDocumentExist');
const {asyncErrHandler} = require('../middleware/helpers');
const jwtVer = require('../middleware/jwtVer');
const passwordValidation = require('../middleware/passwordValidation');

route.get('/me',
    asyncErrHandler(jwtVer),
    asyncErrHandler(doesDocumentExist(User)),
    asyncErrHandler(getMe));

route.delete('/me',
    asyncErrHandler(jwtVer),
    asyncErrHandler(doesDocumentExist(User)),
    asyncErrHandler(deleteMe));

route.patch('/me',
    asyncErrHandler(jwtVer),
    asyncErrHandler(passwordValidation),
    asyncErrHandler(doesDocumentExist(User)),
    asyncErrHandler(changePassword));

module.exports = route;
