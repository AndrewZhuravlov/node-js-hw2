/* eslint-disable new-cap */
const {Router} = require('express');
const {createNote, getAllUserNotes} = require('../controllers/notes/notes');
const {
  getUsersNoteById,
  updateNote,
  checkUncheckNote,
  deleteNote,
} = require('../controllers/notes/notesById');
const Note = require('../models/note');
const User = require('../models/user');
const doesDocumentExist = require('../middleware/doesDocumentExist');
const {asyncErrHandler} = require('../middleware/helpers');
const jwtVer = require('../middleware/jwtVer');
const textValidation = require('../middleware/textValidation');
const route = Router();

route.post('/',
    asyncErrHandler(textValidation),
    asyncErrHandler(jwtVer),
    asyncErrHandler(doesDocumentExist(User)),
    asyncErrHandler(createNote));

route.get('/',
    asyncErrHandler(jwtVer),
    asyncErrHandler(doesDocumentExist(User)),
    asyncErrHandler(getAllUserNotes));

route.get('/:id',
    asyncErrHandler(jwtVer),
    asyncErrHandler(doesDocumentExist(User)),
    asyncErrHandler(getUsersNoteById));

route.put('/:id',
    asyncErrHandler(textValidation),
    asyncErrHandler(jwtVer),
    asyncErrHandler(doesDocumentExist(Note, true)),
    asyncErrHandler(updateNote));

route.patch('/:id',
    asyncErrHandler(jwtVer),
    asyncErrHandler(doesDocumentExist(Note, true)),
    asyncErrHandler(checkUncheckNote));

route.delete('/:id',
    asyncErrHandler(jwtVer),
    asyncErrHandler(doesDocumentExist(Note, true)),
    asyncErrHandler(deleteNote));

module.exports = route;
